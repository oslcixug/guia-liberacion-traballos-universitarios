# Guía para a liberación de traballos universitarios

## O propósito desta guía é alentar aos estudantes a que liberen e compartan os seus traballos universitarios. Baseándose na premisa de que o coñecemento potenciase ao ser compartido, a liberación de traballos por parte das persoas que forman a comunidade universitaria cataliza a innovación e a creatividade.

Esta iniciativa enmárcase no Plan de Acción de Software Libre 2023 da Xunta de Galicia, dentro das liñas de actuación para mellorar e potenciar os mecanismos de formación e difusión do software de fontes abertas entre a cidadanía galega e en particular no estudando universitario.

[![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

Os contidos deste repositorio están dispoñibles baixo unha licenza
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
---
![entidades amtega e cixug](imaxes/logo_amtega_cixug.png)

